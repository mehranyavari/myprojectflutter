import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryIconTheme: IconThemeData(color: Colors.black),
      ),
      title: 'instagram',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xfff8faf8),
        elevation: 1.0,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 12.0),
            child: Icon(
              Icons.send,
            ),
          )
        ],
        leading: Icon(
          Icons.camera_alt,
          color: Colors.black,
        ),
        title: Image.asset(
          'assets/images/logo.png',
          width: 100,
        ),
        centerTitle: true,
      ),
      bottomNavigationBar: Container(
        height: 50.0,
        color: Colors.white,
        child: BottomAppBar(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.home),
                color: Colors.black,
                onPressed: null,
              ),
              IconButton(
                icon: Icon(Icons.search),
                color: Colors.black,
                onPressed: null,
              ),
              IconButton(
                icon: Icon(Icons.add_box),
                color: Colors.black,
                onPressed: null,
              ),
              IconButton(
                icon: Icon(Icons.favorite),
                onPressed: null,
                color: Colors.black,
              ),
              IconButton(
                icon: Icon(Icons.account_box),
                color: Colors.black,
                onPressed: null,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
